class CreateZones < ActiveRecord::Migration[5.0]
  def change
    create_table :zones do |t|
      t.text :nombre

      t.timestamps
    end
  end
end
