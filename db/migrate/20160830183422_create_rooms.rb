class CreateRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :rooms do |t|
      t.text :nombre
      t.integer :capacidad
      t.text :direccion
      t.text :contacto_sala
      t.text :correo_contacto
      t.references :district, foreign_key: true

      t.timestamps
    end
  end
end
