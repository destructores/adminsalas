Rails.application.routes.draw do
  root 'home#principal'

  resources :vendors
  resources :rooms
  resources :districts
  resources :zones
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
