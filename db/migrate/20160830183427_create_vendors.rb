class CreateVendors < ActiveRecord::Migration[5.0]
  def change
    create_table :vendors do |t|
      t.text :nombre
      t.text :apellido
      t.text :telefono_movil
      t.text :correo
      t.references :zone, foreign_key: true

      t.timestamps
    end
  end
end
