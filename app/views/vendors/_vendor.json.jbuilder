json.extract! vendor, :id, :nombre, :apellido, :telefono_movil, :correo, :zone_id, :created_at, :updated_at
json.url vendor_url(vendor, format: :json)