json.extract! room, :id, :nombre, :capacidad, :direccion, :contacto_sala, :correo_contacto, :district_id, :created_at, :updated_at
json.url room_url(room, format: :json)